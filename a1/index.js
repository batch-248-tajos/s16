
const num1 = 15
const num2 = 15

const num3 = 100
const num4 = 100

const num5 = 9
const num6 = 2

console.log('The result of num1 + num2 should be ' + (num1+num2))
console.log('Actual Result: ')
console.log(num1+num2)

console.log('The result of num3 + num4 should be ' + (num3+num4))
console.log('Actual Result: ')
console.log(num3+num4)

console.log('The result of num5 - num6 should be ' + (num5-num6))
console.log('Actual Result: ')
console.log(num5-num6)

// minutes in a year
const resultMinutes = 1440 * 365

// celsius to fahrenheit
const celc = 132
const resultFahrenheit = celc * 1.8 + 32

// is divisible by 8 ?
const num = 165
const rem = num % 8
const isDivisibleBy8 = rem === 0

// is divisible by 4 ?
const num_4 = 348
const rem2 = num_4 % 4
const isDivisibleBy4 = rem2 === 0

// logs
console.log('There are ' + resultMinutes + ' minutes in a year.')
console.log(celc + ' degrees Celsius when converted to Farenheit is ' + resultFahrenheit)
console.log('The remainder of ' + num + ' divided by 8 is: ' + rem)
console.log('Is num7 divisible by 8?')
console.log(isDivisibleBy8)
console.log('The remainder of ' + num_4 + ' divided by 4 is: ' + rem2)
console.log('Is num8 divisible by 4?')
console.log(isDivisibleBy4)